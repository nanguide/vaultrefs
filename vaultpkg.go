package vaultrefs

// VaultCreateTokenResp Json structure of response on the create vault token
type VaultCreateTokenResp struct {
	Auth VaultCreateTokenRespAuth `json:"auth"`
}

// VaultCreateTokenRespAuth Json structure of auth part of response on the create vault token
type VaultCreateTokenRespAuth struct {
	ClientToken   string   `json:"client_token"`
	Accessor      string   `json:"accessor"`
	Policies      []string `json:"policies"`
	LeaseDuration int      `json:"lease_duration"`
	Renewable     bool     `json:"renewable"`
}

// VaultSecretAnswer Json structure of response on the getting secret
type VaultSecretAnswer struct {
	Data VaultSecretAnswerData `json:"data"`
}

// VaultSecretAnswerData Json structure of data in responce on the getting secret
type VaultSecretAnswerData struct {
	Value string `json:"value"`
}
